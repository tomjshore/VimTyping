#include <QtTest>
#include "../typing/typing.h"

// add necessary includes here

class TypingTests : public QObject
{
    Q_OBJECT

public:
    TypingTests();
    ~TypingTests();

private slots:
    void test_case1();

};

TypingTests::TypingTests()
{

}

TypingTests::~TypingTests()
{

}

void TypingTests::test_case1()
{
    //given
    Typing typing = Typing();

    //when
    QString result = typing.Bob();

    //then
    QCOMPARE(result,"bob");
}

QTEST_APPLESS_MAIN(TypingTests)

#include "tst_typingtests.moc"
