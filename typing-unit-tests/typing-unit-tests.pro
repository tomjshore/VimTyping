QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_typingtests.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-typing-Desktop-Debug/release/ -ltyping
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-typing-Desktop-Debug/debug/ -ltyping
else:unix: LIBS += -L$$PWD/../build-typing-Desktop-Debug/ -ltyping

INCLUDEPATH += $$PWD/../build-typing-Desktop-Debug
DEPENDPATH += $$PWD/../build-typing-Desktop-Debug

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-typing-Desktop-Debug/release/libtyping.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-typing-Desktop-Debug/debug/libtyping.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-typing-Desktop-Debug/release/typing.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-typing-Desktop-Debug/debug/typing.lib
else:unix: PRE_TARGETDEPS += $$PWD/../build-typing-Desktop-Debug/libtyping.a
