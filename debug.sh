#! /bin/bash

qmake typing/typing.pro -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug

rm -rf build-typing-Desktop-Debug
mkdir build-typing-Desktop-Debug

cd typing
make

mv *.a ../build-typing-Desktop-Debug
mv *.o ../build-typing-Desktop-Debug
cd ../app

pwd
qmake -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug

make
mv app ..
cd ..



