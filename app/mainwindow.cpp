#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "../typing/typing.h";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    Typing t = Typing();
    QString bob = t.Bob();
    ui->setupUi(this);
    ui->label->setText(bob);
}

MainWindow::~MainWindow()
{
    delete ui;
}
